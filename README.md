ansible-role-facts
=============

This role creates custom facts and gathers application facts using Ansible. These facts are pushed into a PostgreSQL database. Use the data to display facts about your hosts in for example Grafana.

```mermaid
graph LR;
    ansible-facts --> json --> PostgreSQL --> Grafana;
```
Apart from the standard Ansible facts, facts from the following applications will be gathered:
- lldp (package lldpd required)
- apt updates
- PostgreSQL
- MySQL
- Docker
- Restic

Requirements
------------
Add to your requirements.yml file:

```yaml
collections:
  - name: community.docker
    version: 3.10.4
  - name: community.general
    version: 9.5.1
  - name: community.mysql
    version: 3.11.0
  - name: community.clickhouse
    version: 0.6.0
```

Role Variables
--------------
```yaml
custom_facts:
  environment: "{{ env | default('production') }}"
  service: "{{ service }}"
  description: "{{ description | default('') }}"
  management_address: "{{ management_address | default('') }}"
  asset_tag: "{{ asset_tag | default('') }}"
  urls: "{{ urls | default([]) }}"
  tags: "{{ tags | default([]) }}"
```

Example Playbook
----------------


```yaml
---
- name: Gather and create facts
  hosts: all
  become: true
  tasks:
    - include_role:
        name: facts
        apply:
          tags: facts
      tags: always
...
```

Create ssh config file
----------------------
```bash
ansible-playbook playbooks/facts.yml --extra-vars "ssh_config_file=true"
```

Get aws cost and usage
----------------------
```bash
ansible-playbook playbooks/facts.yml --extra-vars "aws_get_cost_and_usage=true"
```


## License

Apache License 2.0

## Author Information

* Rudi Broekhuizen


